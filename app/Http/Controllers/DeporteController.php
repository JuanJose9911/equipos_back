<?php

namespace App\Http\Controllers;

use App\Models\Deporte;
use App\Models\Jugador;
use App\Models\Posicion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DeporteController extends Controller
{


    public function store(Request $request)
    {
        try {
            //en el validator ponerle mimes:svg al icono
            $mensajes = $this->mensajesError();
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'icono' => 'required'
            ], $mensajes);
            if ($validator->fails()) {
                $messages = $validator->messages();
                return response()->json([
                    'mensajes' => $messages,
                    'successfull' => false
                ], 200);
            }
            $deporte = Deporte::create(array_merge(
                $validator->validate(),
                [
                    'icono' => $this->guardarImagen($request->icono)
                ]
            ));
            return response()->json([
                'message' => '¡Deporte registrado correctamente!',
                'deporte' => $deporte,
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function index()
    {
        try {
            $deportes = Deporte::withCount('equipos')
                ->withCount('posiciones')
                ->with('posiciones')
                ->get();
            foreach ($deportes as $deporte) {
                foreach ($deporte['posiciones'] as $key => $value) {
                    $value->disabled = 1;
                    $value->cantidadJugadores = DB::table('jugadores')->where('posicion_id',$value->id)->count();
                }
                $deporte->icono = env('APP_URL'). $deporte->icono;
            }
            return $deportes;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function update(Request $request, int $id)
    {
        try {
            $mensajes = $this->mensajesError();
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'icono' => 'required'
            ], $mensajes);
            if ($validator->fails()) {
                $messages = $validator->messages();
                return response()->json([
                    'mensajes' => $messages,
                    'successfull' => false
                ], 200);
            }
            $deporte = Deporte::findOrFail($id);
            $deporte = $deporte->update([
                'icono' => $this->guardarImagen($request->icono),
                'nombre' => $request->nombre
            ]);
            return response()->json([
                'message' => 'Datos actualizados correctamente',
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function destroy($id)
    {
        try {
            $deporte = Deporte::findOrFail($id);
            $deporte->delete();
            return response()->json([
                'message' => 'Deporte eliminado correctamente',
                'successfull' => true
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
