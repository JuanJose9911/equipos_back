<?php

namespace App\Http\Controllers;

use App\Models\Entrenador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EntrenadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // try {
        //     $validator = Validator::make($request->all(), [
        //         'imagen' => 'required',
        //         'nombres' => 'required',
        //         'apellidos' => 'required',

        //     ]);
        //     if ($validator->fails()) {
        //         return response()->json($validator->errors()->toJson(), 400);
        //     }
        //     $entrenador = Entrenador::create(array_merge(
        //         $validator->validate(),
        //         [
        //             'equipos_id' => $request->equipos_id,
        //             'imagen' => $this->guardarImagen($request->imagen),
        //             'created_by' => $request->created_by
        //         ]
        //     ));
        //     return response()->json([
        //         'message' => 'Entrenador registrado exitosamente',
        //         'entrenador' => $entrenador
        //     ], 201);
        // } catch (\Throwable $th) {
        //     throw $th;
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Entrenador  $entrenador
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $entrenador = Entrenador::select('nombres', 'apellidos','imagen')
            ->where('equipos_id', $id)
            ->first();
            $entrenador->imagen =  env('APP_URL') . $entrenador->imagen;
            return $entrenador;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Entrenador  $entrenador
     * @return \Illuminate\Http\Response
     */
    public function update(int $id, Request $request)
    {
        /* //primero encontrar al admin y luego usar update()
        try {
            $validator = Validator::make($request->all(), [
                'imagen' => 'required',
                'nombres' => 'required',
                'apellidos' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors()->toJson(), 400);
            }
            $usuario = Entrenador::findOrFail($id);
            
            $usuario = $usuario->update([
                'imagen' => $this->guardarImagen($request->imagen),
                'nombres' => $request->nombres,
                'apellidos' => $request->apellidos
            ]);

            return response()->json([
                'message' => 'Datos actualizados correctamente',
                'user' => $usuario,
            ], 201);
            
        } catch (\Throwable $th) {
            throw $th;
        } */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Entrenador  $entrenador
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entrenador $entrenador)
    {
        //
    }
}
