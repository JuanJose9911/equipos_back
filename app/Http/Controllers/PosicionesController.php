<?php

namespace App\Http\Controllers;

use App\Models\Posicion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PosicionesController extends Controller
{
    public function store(Request $request)
    {
        $mensajes = $this->mensajesError();
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:20'
        ], $mensajes);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'mensajes' => $messages,
                'successfull' => false
            ], 200);
        }
        $posicion = Posicion::create(array_merge(
            $validator->validate(),
            [
                'deporte_id' => $request->deporte_id
            ]
        ));
        return response()->json([
            'message' => '¡Posicion registrado correctamente!',
            'deporte' => $posicion,
            'successfull' => true
        ], 201);
    }

    public function index($id)
    {
        try {
            $posiciones = DB::table('posiciones')
                ->select(
                    'posiciones.id',
                    'posiciones.nombre',
                    'posiciones.deporte_id'
                )
                ->where('deporte_id', $id)
                ->get();
            return $posiciones;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function update(Request $request, int $id)
    {
        try {
            $mensajes = $this->mensajesError();
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
            ], $mensajes);
            if ($validator->fails()) {
                $messages = $validator->messages();
                return response()->json([
                    'mensajes' => $messages,
                    'successfull' => false
                ], 200);
            }
            $posicion = Posicion::findOrFail($id);
            $posicion = $posicion->update([
                'nombre' => $request->nombre
            ]);
            return response()->json([
                'message' => 'Datos actualizados correctamente',
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function destroy(int $id)
    {
        try {
            $posicion = Posicion::findOrFail($id);
            $posicion->delete();
            return response()->json([
                'message' => 'Posicion eliminada correctamente',
                'successfull' => true
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
