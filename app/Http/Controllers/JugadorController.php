<?php

namespace App\Http\Controllers;

use App\Models\Jugador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class JugadorController extends Controller
{
    public function registrarJugador(Request $request)
    {
        try {
            $mensajes = $this->mensajesError();
            $validator = Validator::make($request->all(), [
                'imagen' => 'required',
                'nombres' => 'required',
                'apellidos' => 'required',
                'equipo_id' => 'required',
                'nivelDeportivo' => 'required',
                'posicion_id' => 'required'
            ], $mensajes);
            if ($validator->fails()) {
                $messages = $validator->messages();
                return response()->json([
                    'mensajes' => $messages,
                    'successfull' => false
                ], 200);
            };
            $jugador = Jugador::create(array_merge(
                $validator->validate(),
                [
                    'imagen' => $this->guardarImagen($request->imagen),
                    'posicion_id' => $request->posicion_id,
                ]
            ));
            return response()->json([
                'message' => 'Jugador registrado correctamente!',
                'jugador' => $jugador,
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function update(int $id, Request $request)
    {
        try {
            $mensajes = $this->mensajesError();
            $validator = Validator::make($request->all(), [
                'imagen' => 'required',
                'nombres' => 'required',
                'apellidos' => 'required',
                'nivelDeportivo' => 'required',
                'posicion_id' => 'required'
            ], $mensajes);
            if ($validator->fails()) {
                $messages = $validator->messages();
                return response()->json([
                    'mensajes' => $messages,
                    'successfull' => false
                ], 200);
            }

            $jugador = Jugador::findOrFail($id);
            $jugador = $jugador->update([
                'imagen' => $this->guardarImagen($request->imagen),
                'nombres' => $request->nombres,
                'apellidos' => $request->apellidos,
                'nivelDeportivo' => $request->nivelDeportivo,
                'posicion_id' => $request->posicion_id
            ]);


            return response()->json([
                'message' => 'Datos actualizados correctamente',
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function show($id)
    {
        try {
            $jugadores = DB::table('jugadores')
                ->join('posiciones', 'jugadores.posicion_id', 'posiciones.id')
                ->join('equipos', 'jugadores.equipo_id', 'equipos.id')
                ->select(
                    'jugadores.id',
                    'jugadores.nombres',
                    'jugadores.apellidos',
                    'jugadores.imagen',
                    'jugadores.nivelDeportivo',
                    'jugadores.posicion_id',
                    'posiciones.nombre as nomPosic',
                    'equipos.id as id_equipo'
                )
                ->where('equipo_id', $id)
                ->get();
            $promedioJugadores = 0;
            $total = $jugadores->count();
            if ($total > 0) {
                foreach ($jugadores as $jugador) {
                    $promedioJugadores += $jugador->nivelDeportivo;
                }
                $promedioJugadores = $promedioJugadores / $total;
            }
            foreach ($jugadores as $jugador) {
                $jugador->imagen =  env('APP_URL') . $jugador->imagen;
            }
            return [
                'jugadores' => $jugadores, 
                'cantidad' => $promedioJugadores
            ];
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function destroy(int $id)
    {
        try {
            $jugador = Jugador::findOrFail($id);
            $jugador->delete();
            return response()->json([
                'message' => 'Jugador eliminado correctamente',
                'successfull' => true
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
