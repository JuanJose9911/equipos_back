<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CiudadController extends Controller
{
    public function index(int $id)
    {
        try {
            $ciudades = DB::table('ciudades')
                            ->select('id','nombre')
                            ->where('paises_id',$id)
                            ->get();
            
            return $ciudades;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
