<?php

namespace App\Http\Controllers;

use App\Models\Entrenador;
use App\Models\Equipo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use function PHPUnit\Framework\isEmpty;

class EquipoController extends Controller
{
    public function store(Request $request)
    {
        try {
            $mensajes = $this->mensajesError();
            $validator = Validator::make($request->all(), [
                'imagen' => 'required',
                'nombre' => 'required',
                'descripcion' => 'required',
                'ciudades_id' => 'required',
                'deporte_id' => 'required',
                'imagen_coach' => 'required',
                'nombres_coach' => 'required',
                'apellidos_coach' => 'required'
            ], $mensajes);
            if ($validator->fails()) {
                $messages = $validator->messages();
                return response()->json([
                    'mensajes' => $messages,
                    'successfull' => false
                ], 200);
            }
            DB::transaction(function () use ($validator, $request) {
                $entrenador = Entrenador::create(
                    [
                        'nombres' => $request->nombres_coach,
                        'apellidos' => $request->apellidos_coach,
                        'imagen' => $this->guardarImagen($request->imagen_coach)
                    ]
                );
                $id_entrenador = $entrenador->id;
                $equipo = Equipo::create(array_merge(
                    $validator->validate(),
                    [
                        'imagen' => $this->guardarImagen($request->imagen),
                        'nombre' => $request->nombre,
                        'entrenador_id' => $id_entrenador,
                        'ciudades_id' => $request->ciudades_id,
                        'deporte_id' => $request->deporte_id
                    ]
                ));
                return [$equipo, $entrenador];
            });
            return response()->json([
                'message' => 'Equipo registrado correctamente!',
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function index()
    {
        try {
            $equipos = Equipo::select(
                'equipos.id',
                'equipos.nombre',
                'equipos.imagen',
                'deportes.id as idDeporte',
                'deportes.icono as imagenDeporte'
            )
                ->join('deportes', 'equipos.deporte_id', 'deportes.id')
                ->withCount('jugadores as numJugadores')->get();
            foreach ($equipos as $equipo) {
                $equipo->imagen = env('APP_URL') . $equipo->imagen;
                $equipo->imagenDeporte = env('APP_URL') . $equipo->imagenDeporte;
            }
            return response()->json([
                'message' => 'Datos listados correctamente',
                'successfull' => true,
                'equipos' => $equipos,
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function show(int $id)
    {
        try {
            $equipo = DB::table('equipos')
                ->join('entrenadores', 'equipos.entrenador_id', 'entrenadores.id')
                ->join('deportes', 'equipos.deporte_id', 'deportes.id')
                ->join('ciudades', 'equipos.ciudades_id', 'ciudades.id')
                ->join('paises', 'ciudades.paises_id', 'paises.id')
                ->where('equipos.id', $id)
                ->select(
                    'equipos.id',
                    'equipos.nombre',
                    'equipos.descripcion',
                    'equipos.imagen',
                    'entrenadores.id as idEntrenador',
                    'entrenadores.nombres',
                    'entrenadores.apellidos',
                    'entrenadores.imagen as imagenCoach',
                    'ciudades.id as idCiudad',
                    'ciudades.nombre as ciudad',
                    'paises.id as idPais',
                    'paises.nombre as pais',
                    'deportes.id as idDeporte',
                    'deportes.nombre as deporte',
                    'deportes.icono as imgDeporte'
                )
                ->get();
            foreach ($equipo as $equipo) {
                $equipo->imagen = env('APP_URL') . $equipo->imagen;
                $equipo->imagenCoach = env('APP_URL') . $equipo->imagenCoach;
                $equipo->imgDeporte = env('APP_URL') . $equipo->imgDeporte;
            }
            return response()->json([
                'successfull' => true,
                'equipo' => $equipo,
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    //Editar imagen coach
    public function editImgCoach(int $id, Request $request)
    {
        try {
            $equipoExist = Equipo::findOrFail($id)->exists();
            if ($equipoExist) {
                $entrenador = Entrenador::findOrFail($request->entrenadores_id);
                if ($request->imagen_coach == null) {
                    $imgDefault = "/storage/iconoAdmins.svg";
                    DB::transaction(function () use ($imgDefault, $entrenador) {
                        $entrenador = $entrenador->update([
                            'imagen' => $imgDefault
                        ]);
                    });
                    return response()->json([
                        'message' => 'Coach actualizados correctamente',
                        'successfull' => true
                    ], 201);
                }else{
                    $entrenador = $entrenador->update([
                        'imagen' => $this->guardarImagen($request->imagen_coach),
                    ]);
                    return response()->json([
                        'message' => 'Datos actualizados correctamente',
                        'successfull' => true
                    ], 201);
                }
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Editar imagen equipo
    public function editImgEquipo(int $id, Request $request)
    {

        try {
            $equipoExist = Equipo::findOrFail($id)->exists();
            if ($equipoExist) {
                $equipo = Equipo::findOrFail($id);
                if ($request->imagen == null) {
                    $imgDefault = "/storage/iconoEquipos.svg";
                    DB::transaction(function () use ($imgDefault, $equipo) {
                        $equipo = $equipo->update([
                            'imagen' => $imgDefault
                        ]);
                    });
                    return response()->json([
                        'message' => 'Datos actualizados correctamente',
                        'successfull' => true
                    ], 201);
                } else {
                    $equipo = $equipo->update([
                        'imagen' => $this->guardarImagen($request->imagen),
                    ]);
                    return response()->json([
                        'message' => 'Datos actualizados correctamente',
                        'successfull' => true
                    ], 201);
                }
            } else {
                return response()->json([
                    'message' => 'Usuario no encontrado',
                    'successfull' => false
                ], 201);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function update(int $id, Request $request)
    {
        //primero encontrar al equipo y luego usar update()
        try {
            $mensajes = $this->mensajesError();
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'descripcion' => 'required|string|max:500',
                'ciudades_id' => 'required',
                'deporte_id' => 'required',
                'entrenadores_id' => 'required',
                'nombres_coach' => 'required',
                'apellidos_coach' => 'required',
            ], $mensajes);
            if ($validator->fails()) {
                $messages = $validator->messages();
                return response()->json([
                    'mensajes' => $messages,
                    'successfull' => false
                ], 200);
            }

            DB::transaction(function () use ($request, $id) {
                $entrenador = Entrenador::findOrFail($request->entrenadores_id);
                $entrenador = $entrenador->update([
                    'nombres' => $request->nombres_coach,
                    'apellidos' => $request->apellidos_coach
                ]);

                $equipo = Equipo::findOrFail($id);
                $equipo = $equipo->update([
                    'nombre' => $request->nombre,
                    'descripcion' => $request->descripcion,
                    'ciudades_id' => $request->ciudades_id,
                    'deporte_id' => $request->deporte_id
                ]);
            });


            return response()->json([
                'message' => 'Datos actualizados correctamente',
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function destroy(int $id)
    {
        try {
            $equipo = Equipo::findOrFail($id);
            $equipo->delete();
            return response()->json([
                'message' => 'Equipo eliminado correctamente',
                'successfull' => true
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
