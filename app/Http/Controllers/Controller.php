<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function guardarImagen($imagenB64)
    {
        $image_64 = $imagenB64; //your base64 encoded data
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf    
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        // find substring fro replace here eg: data:image/png;base64,    
        $image = str_replace($replace, '', $image_64);
        $image = str_replace(' ', '+', $image);
        $imageName = Str::random(10) . '.' . $extension;
        $imagen = Storage::url($imageName);
        $url_img = Storage::disk('public')->put($imageName, base64_decode($image));

        return $imagen;
    }

    public function mensajesError()
    {
        $mensajes = array(
            'required' => 'Este campo es obligatorio',
            'email' => 'Debes introducir una direccion de correo valida',
            'digits' => 'El numero de telefono debe ser de 10 digitos',
            'unique' => 'Este correo ya esta en uso',
            'min' => 'La contraseña debe tener almenos 8 caracteres'
        );

        return $mensajes;
    }
}
