<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['correo', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'No autorizado'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(env('APP_URL') . auth()->user()->imagen);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
    public function register(Request $request)
    {
        $mensajes = $this->mensajesError();
        $validator = Validator::make($request->all(), [
            'imagen' => 'required',
            'nombres' => 'required|string',
            'apellidos' => 'required|string',
            'correo' => 'required|string|email|max:100|unique:users',
            'password' => 'required|min:8|alpha_dash|confirmed',
            'password_confirmation' => 'required',
            'acercaAdmin' => 'required|string|max:1000',
            'telefono' => 'required|numeric|digits:10'
        ], $mensajes);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'mensajes' => $messages,
                'successfull' => false
            ], 200);
        }
        $user = User::create(array_merge(
            $validator->validate(),
            [
                'password' => bcrypt($request->password),
                'imagen' => $this->guardarImagen($request->imagen)
            ]
        ));
        return response()->json([
            'message' => '¡Usuario registrado correctamente!',
            'user' => $user,
            'successfull' => true
        ], 201);
    }
    //listar todos los admins
    public function index()
    {
        try {
            $usuarios = User::get();
            foreach ($usuarios as $usuario) {
                $usuario->imagen = env('APP_URL') . $usuario->imagen;
            }
            return $usuarios;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    //listar 1 admin
    public function show(int $id)
    {
        try {
            $usuario = User::find($id);
            if ($usuario) {
                $usuario->imagen = env('APP_URL') . $usuario->imagen;
                return response()->json([
                    'user' => $usuario,
                    'successfull' => true
                ], 201);
            } else {
                return response()->json([
                    'user' => $usuario,
                    'successfull' => false
                ], 201);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    public function editImgAdmin(int $id, Request $request)
    {

        try {
            $userExist = User::findOrFail($id)->exists();
            if ($userExist) {
                $usuario = User::findOrFail($id);
                if ($request->imagen == null) {
                    $imgDefault = "/storage/iconoAdmins.svg";
                    DB::transaction(function () use ($imgDefault, $usuario) {
                        $usuario = $usuario->update([
                            'imagen' => $imgDefault
                        ]);
                    });
                    return response()->json([
                        'message' => 'Datos actualizados correctamente',
                        'successfull' => true
                    ], 201);
                } else {
                    $usuario = $usuario->update([
                        'imagen' => $this->guardarImagen($request->imagen),
                    ]);
                    return response()->json([
                        'message' => 'Datos actualizados correctamente',
                        'successfull' => true
                    ], 201);
                }
            } else {
                return response()->json([
                    'message' => 'Usuario no encontrado',
                    'successfull' => false
                ], 201);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Actualizar info de 1 admin
    public function update(int $id, Request $request)
    {
        //primero encontrar al admin y luego usar update()
        try {
            $mensajes = $this->mensajesError();
            $validator = Validator::make($request->all(), [
                'nombres' => 'required',
                'apellidos' => 'required',
                'correo' => 'required|string|email|max:100',
                'password' => 'required|min:6',
                'acercaAdmin' => 'required|string|max:500',
                'telefono' => 'required|string|min:7|max:10'
            ], $mensajes);
            if ($validator->fails()) {
                $messages = $validator->messages();
                return response()->json([
                    'mensajes' => $messages,
                    'successfull' => false
                ], 200);
            }
            $usuario = User::findOrFail($id);
            $usuario = $usuario->update([
                'acercaAdmin' => $request->acercaAdmin,
                'apellidos' => $request->apellidos,
                'correo' => $request->correo,
                'nombres' => $request->nombres,
                'password' =>  bcrypt($request->password),
                'telefono' => $request->telefono
            ]);
            return response()->json([
                'message' => 'Datos actualizados correctamente',
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    //Eliminar 1 admin
    public function destroy(int $id)
    {
        try {
            $usuario = User::findOrFail($id);
            $usuario->delete();
            return response()->json([
                'message' => 'Usuario eliminado correctamente',
                'successfull' => true
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
