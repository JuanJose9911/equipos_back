<?php

namespace App\Http\Controllers;

use App\Models\Pais;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PaisController extends Controller
{
    public function index()
    {
        try {
            //$paises = Pais::get();
            //foreach concatenar la url total a la de la imagen
            $paises = DB::table('paises')
                            ->select('id','nombre')
                            ->get();
            
            return $paises;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
