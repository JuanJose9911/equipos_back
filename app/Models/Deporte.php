<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Posicion;
use App\Models\Equipo;

class Deporte extends Model
{
    use HasFactory;
    protected $table = "deportes";

    protected $fillable = [
        'nombre',
        'icono'
    ];

    //Deporte_posiciones
    public function posiciones(){
        return $this->hasMany(Posicion::class);
    }

    //Deporte_equipos
    public function equipos(){
        return $this->hasMany(Equipo::class);
    }
}
