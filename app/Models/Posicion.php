<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Deporte;
use App\Models\Jugador;

class Posicion extends Model
{
    use HasFactory;
    protected $table = "posiciones";

    protected $fillable = [
        'nombre',
        'deporte_id'
    ];

    //Posiciones_deporte
    public function deporte(){
        return $this->belongsTo(Deporte::class, 'deporte_id');
    }

    //Posiciones_jugadores
    public function jugadores(){
        return $this-> belongsTo(Jugador::class);
    }
}
