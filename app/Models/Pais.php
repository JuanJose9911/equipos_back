<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Ciudad;

class Pais extends Model
{
    use HasFactory;
    protected $table = "paises";

    protected $fillable = [
        'nombre',
    ];
    //Paises_ciudades
    public function ciudades(){
        return $this->hasMany(Ciudad::class);
    }
}
