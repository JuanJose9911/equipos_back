<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Jugador;
use App\Models\Entrenador;
use App\Models\Deporte;
use App\Models\Ciudad;

class Equipo extends Model
{
    use HasFactory;
    protected $table = "equipos";

    protected $fillable = [
        'imagen',
        'nombre',
        'descripcion',
        'entrenador_id',
        'ciudades_id',
        'deporte_id'
    ];

    //Equipos_jugadores
    public function jugadores(){
        return $this->hasMany(Jugador::class);
    }

    //Equipos_entrenador
    public function entrenador(){
        return $this->hasOne(Entrenador::class);
    }

    //Equipos_deporte
    public function deporte(){
        return $this->belongsTo(Deporte::class, 'deportes_id');
    }

    //Equipos_ciudades
    public function ciudad(){
        return $this->hasOne(Ciudad::class);
    }
}
