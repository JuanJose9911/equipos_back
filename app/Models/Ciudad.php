<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pais;

class Ciudad extends Model
{
    use HasFactory;

     //Ciudades_pais
     public function pais(){
        return $this-> belongsTo(Pais::class, 'paises_id');
    }

    //Ciudad_equipos
    public function equipo()
    {

        return $this->belongsTo(Equipo::class,);
    }
}
