<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Equipo;
//use Wildside\Userstamps\Userstamps;

class Entrenador extends Model
{
    use HasFactory;
   // use Userstamps;

    protected $table = "entrenadores";

    protected $fillable = [
        'imagen',
        'nombres',
        'apellidos',
        'created_by',
        'equipos_id'
            
    ];

    //Entrenadores_equipos
    public function equipo(){
        return $this->belongsTo(Equipo::class);
    }
}
