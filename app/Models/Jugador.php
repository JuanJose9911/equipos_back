<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Posicion;
use App\Models\Equipo;

class Jugador extends Model
{
    use HasFactory;
    protected $table = "jugadores";

    protected $fillable = [
        'nombres',
        'apellidos',
        'imagen',
        'nivelDeportivo',
        'equipo_id',
        'posicion_id'
    ];
    //Jugadores_posiciones
    public function posiciones(){
        return $this->hasOne(Posicion::class);
    }

    //Jugadores_equipos
    public function equipo(){
        return $this-> belongsTo(Equipo::class, 'equipos_id');
    }
}
