<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    //Admins
    //Registro, login
    Route::post('login', 'App\Http\Controllers\AuthController@login')->name('login');
    Route::post('logout', 'App\Http\Controllers\AuthController@logout');
    Route::post('refresh', 'App\Http\Controllers\AuthController@refresh');
    Route::post('register', 'App\Http\Controllers\AuthController@register');
    Route::post('me', 'App\Http\Controllers\AuthController@me');
    //Listar admins
    Route::get('administradores', 'App\Http\Controllers\AuthController@index');
    Route::get('administradores/{id}', 'App\Http\Controllers\AuthController@show');
    //Editar admins
    Route::put('editar-administradores/{id}', 'App\Http\Controllers\AuthController@update');
    //Borrar admins
    Route::delete('borrar-administradores/{id}', 'App\Http\Controllers\AuthController@destroy');
    //eliminar imagen 
    Route::put('editar-imagen/{id}','App\Http\Controllers\AuthController@editImgAdmin');





    //deporte
    Route::post('registrar-deporte', 'App\Http\Controllers\DeporteController@store');
    Route::get('deportes', 'App\Http\Controllers\DeporteController@index');
    Route::put('editar-deporte/{id}', 'App\Http\Controllers\DeporteController@update');
    Route::delete('eliminar-deporte/{id}', 'App\Http\Controllers\DeporteController@destroy');


    //jugadores
    Route::post('registrar-jugador', 'App\Http\Controllers\JugadorController@registrarJugador');
    Route::get('jugadores/{id}', 'App\Http\Controllers\JugadorController@show');
    Route::delete('eliminar-jugador/{id}', 'App\Http\Controllers\JugadorController@destroy');
    Route::put('editar-jugador/{id}', 'App\Http\Controllers\JugadorController@update');




    //equipos
    Route::post('crear-equipo', 'App\Http\Controllers\EquipoController@store');
    Route::get('equipos', 'App\Http\Controllers\EquipoController@index');
    Route::get('equipos/{id}', 'App\Http\Controllers\EquipoController@show');
    Route::delete('eliminar-equipos/{id}', 'App\Http\Controllers\EquipoController@destroy');
    Route::put('editar-equipo/{id}', 'App\Http\Controllers\EquipoController@update');
    //eliminar imagen 
    Route::put('editar-imagen/{id}','App\Http\Controllers\EquipoController@editImgEquipo');
    Route::put('editar-imagen-coach/{id}','App\Http\Controllers\EquipoController@editImgCoach');

    //entrenadores
    Route::post('crear-entrenador', 'App\Http\Controllers\EntrenadorController@store');
    Route::get('entrenador/{id}', 'App\Http\Controllers\EntrenadorController@show');
    Route::put('editar-entrenador/{id}', 'App\Http\Controllers\EntrenadorController@update');


    //posiciones
    Route::post('crear-posicion', 'App\Http\Controllers\PosicionesController@store');
    Route::get('listar-posiciones/{id}', 'App\Http\Controllers\PosicionesController@index');
    Route::put('editar-posicion/{id}', 'App\Http\Controllers\PosicionesController@update');
    Route::delete('eliminar-posicion/{id}', 'App\Http\Controllers\PosicionesController@destroy');


    //paises 
    Route::get('listar-paises', 'App\Http\Controllers\PaisController@index');
    Route::get('listar-ciudades/{id}', 'App\Http\Controllers\CiudadController@index');


});