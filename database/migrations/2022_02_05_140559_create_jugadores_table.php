<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJugadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jugadores', function (Blueprint $table) {
            $table->id();
            $table->string('nombres',45);
            $table->string('apellidos', 45);
            $table->integer('nivelDeportivo');
            $table->string('imagen');
            //Clave foranea posicion
            $table->foreignId('posicion_id')
                    ->nullable()//borrar el null en las claves foraneas
                    ->constrained('posiciones')
                    ->cascadeOnUpdate()
                    ->nullOnDelete();
            //Clave foranea equipo
            $table->foreignId('equipo_id')
                    ->nullable()
                    ->constrained('equipos')
                    ->cascadeOnUpdate()
                    ->cascadeOnDelete();
            //Create && update
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();//created_at updated_at
            $table->softDeletes();//deleted_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jugadores');
    }
}
