<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 45);
            $table->string('descripcion', 1000);
            $table->string('imagen');
            //Clave foranea deportes
            $table->foreignId('deporte_id')
                    ->nullable()
                    ->constrained('deportes')
                    ->cascadeOnUpdate()
                    ->nullOnDelete();
            //Clave foranea entrenador
            $table->foreignId('entrenador_id')
                    ->nullable()
                    ->constrained('entrenadores')
                    ->cascadeOnUpdate()
                    ->cascadeOnDelete();
            //Clave foranea ciudades
            $table->foreignId('ciudades_id')
                    ->nullable()
                    ->constrained('ciudades')
                    ->cascadeOnUpdate()
                    ->nullOnDelete();
            //Creates && updates
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps(); // created_at updated_at
            $table->softDeletes();//deleted_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipos');
    }
}
